/* user and group to drop privileges to */
static const char *user  = "obskyr";
static const char *group = "video";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#4D499D",     /* after initialization */
	[INPUT] =  "#4D499D",   /* during input */
	[FAILED] = "#3C388C",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
